# Maintainer:  Alad Wenter <alad (at) archlinux.info>
# Contributor: Jochen Schalanda <jochen+aur (at) schalanda.name>
# Contributor: C. Dominik Bódi <dominik.bodi@gmx.de>
# Contributor: Pierre Carrier <pierre@spotify.com>
# Contributor: Thomas Dziedzic <gostrc (at) gmail>
# Contributor: Chris Giles <Chris.G.27 (at) Gmail.com>
# Contributor: seblu <seblu+arch (at) seblu.net>
# Contributor: squiddo <squiddo (at) intheocean.net>
# Contributor: dront78 <dront78 (at) gmail.com>
# Contributor: hugelgupf <ckoch (at) cs.nmt.edu>

pkgname=dpkg-ubuntu
_pkgname=dpkg
pkgver=1.19.7
ubuntuver=ubuntu2
pkgrel=1
pkgdesc="The Debian Package Manager.  Don't use it instead of Arch's 'pacman'."
arch=('i686' 'x86_64')
url="https://packages.ubuntu.com/bionic/dpkg"
license=('GPL')
conflicts=('dpkg')
provides=('dpkg' 'dpkg-ubuntu')
replaces=('dpkg')
options=('emptydirs')
depends=('xz' 'zlib' 'bzip2' 'perl')
makedepends=('perl-io-string' 'perl-timedate' 'git')
checkdepends=('perl-io-string' 'perl-test-pod')
install=dpkg.install
source=("http://archive.ubuntu.com/ubuntu/pool/main/d/dpkg/${_pkgname}_${pkgver}${ubuntuver}.tar.xz")
sha256sums=('SKIP')

prepare() {
    cd ${_pkgname}
    echo "$pkgver" > .dist-version
    ./autogen
}

#check() {
#    cd "$pkgname"
#    #make check
#}

build() {
    cd ${_pkgname}
    ./configure --prefix=/usr \
        --sysconfdir=/etc \
        --localstatedir=/var \
        --sbindir=/usr/bin \
        --libexecdir=/usr/lib \
        --disable-start-stop-daemon \
        --disable-devel-docs \
        --without-libmd \
        --with-libz \
        --with-libzstd \
        --with-liblzma \
        --with-libbz2
    make
}

package() {
    cd "${_pkgname}"
    make DESTDIR="$pkgdir" install

    install -d "$pkgdir/var/$_pkgname"/updates/
    touch "$pkgdir/var/lib/$_pkgname"/{status,available}
}
